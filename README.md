For backend Spring Boot you need Java 11.

cd backend
./gradlew build
./gradlew bootRun

Backend application will run on  http://localhost:5000

In-memory H2 database is available at http://localhost:5000/h2-console 
login: test
passw: test

Initial data will be loaded from resources/data.sql

Examples:
http://localhost:5000/customer/1000
http://localhost:5000/customer/1000/parkingTime
http://localhost:5000/parkingHouse/1000
http://localhost:5000/user/1000
http://localhost:5000/user/1000/customer






Frontend is in React

cd frontend
npm install
npm start

Frontend will run on http://localhost:3000

