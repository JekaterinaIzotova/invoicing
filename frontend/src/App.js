import './App.css';
import Header from './parts/Header';
import PageContainer from './parts/PageContainer';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  main: {
    backgroundColor: '#f5f5f5',
    minHeight: '100vh',
  },
}));

function App() {
  const classes = useStyles();
  return (
    <>
      <div className={classes.main}>
        <Header></Header>
        <PageContainer>
          <div></div>
        </PageContainer>
      </div>
    </>
  );
}

export default App;
