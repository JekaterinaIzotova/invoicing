import React, { useRef, useState } from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import logo from '../assets/img/logo.png';
import {
  Toolbar,
  IconButton,
  ButtonGroup,
  Button,
  useTheme,
  Link,
  Dialog,
  Slide,
  List,
  ListItem,
  Popper,
  Paper,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  header: {
    backgroundColor: '#e3f2fd',
    [theme.breakpoints.up('md')]: {
      height: 102,
    },
    zIndex: theme.zIndex.drawer + 1,
  },
  flex: {
    flexGrow: 1,
  },
  logo: {
    marginLeft: 12,
    marginRight: 30,
  },
  customerButton: {
    margin: 8,
    height: 44,
  },
  customerButtonText: {
    opacity: 0.7,
  },
  customerInfoButton: {
    opacity: 0.9,
  },
  customerPanelPopper: {
    zIndex: 3,
  },
  customerPanel: {
    width: 296,
    paddingTop: 10,
    paddingBottom: 10,
  },
  listItem: {},
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

function ListItemLink(props) {
  const { onClick, classes } = props;

  return (
    <>
      <li>
        <ListItem button onClick={onClick} className={classes.listItem}>
          Car number: 123ABC
        </ListItem>
      </li>
      <li>
        <ListItem button onClick={onClick} className={classes.listItem}>
          Car number: 567ABC
        </ListItem>
      </li>
    </>
  );
}

function DesktopHeader() {
  const classes = useStyles();
  const [customerPanelOpen, setCustomerPanelOpen] = useState(false);
  let popperAnchor = useRef(null);

  const toggleCustomerPanel = () => {
    setCustomerPanelOpen(!customerPanelOpen);
  };

  return (
    <>
      <span className={classes.flex}>NAME OF PARKING HOUSE</span>

      <ButtonGroup
        disableElevation
        aria-label='customer info'
        variant='contained'
        color={customerPanelOpen ? 'primary' : 'secondary'}
        className={classes.customerButton}
      >
        <Button
          onClick={toggleCustomerPanel}
          className={classes.customerInfoButton}
          ref={popperAnchor}
          aria-label='select customer'
        >
          <span className={classes.customerButtonText}>CUSTOMER</span>
          &nbsp;
          <span>123ABC</span>
        </Button>
      </ButtonGroup>

      <Popper
        open={customerPanelOpen}
        anchorEl={popperAnchor.current}
        placement='bottom-end'
        modifiers={{ offset: { enabled: true, offset: '0, 34' } }}
        className={classes.customerPanelPopper}
      >
        <Paper elevation={0} className={classes.customerPanel}>
          <List>
            <ListItemLink onClick={toggleCustomerPanel} classes={classes} />
          </List>
        </Paper>
      </Popper>
    </>
  );
}

function MobileHeader({}) {
  const classes = useStyles();
  const [panelOpen, setPanelOpen] = useState(false);

  const handlePanelOpen = () => {
    setPanelOpen(true);
  };
  const handlePanelClose = () => {
    setPanelOpen(false);
  };

  return (
    <>
      <span className={classes.flex}>PARKING HOUSE</span>

      <IconButton onClick={handlePanelOpen} aria-label='open customer info'>
        123ABC
      </IconButton>

      <Dialog
        fullScreen
        open={panelOpen}
        onClose={handlePanelClose}
        TransitionComponent={Transition}
      >
        <IconButton onClick={handlePanelClose} aria-label='close'></IconButton>
        <List>
          <ListItemLink onClick={handlePanelClose} classes={classes} />
        </List>
      </Dialog>
    </>
  );
}

export default function Header() {
  const classes = useStyles();
  const theme = useTheme();
  const desktop = useMediaQuery(theme.breakpoints.up('md'));

  return (
    <AppBar elevation={0} color='transparent' position='static'>
      <Toolbar className={classes.header}>
        <Link to='/' className={classes.logo}>
          <img src={logo} alt='logo' />
        </Link>
        {desktop ? <DesktopHeader /> : <MobileHeader />}
      </Toolbar>
    </AppBar>
  );
}
