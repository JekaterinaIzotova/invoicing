package ee.car.park.invoicing.db.repository;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.ParkingTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ParkingTimeRepository extends JpaRepository<ParkingTime, Long> {

  @Query("select pt from ParkingTime pt where pt.customer = :customer")
  Set<ParkingTime> findAllByCustomer(@Param("customer") Customer customer);

}