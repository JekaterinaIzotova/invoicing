package ee.car.park.invoicing.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter @Setter
@SequenceGenerator(name = "default", sequenceName = "customer_id_seq", allocationSize = 1)
public class Customer extends BaseEntity {

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  private User user;

  @Column(name = "car_number")
  private String carNumber;

  @ManyToOne(fetch = FetchType.LAZY)
  private CustomerGroupFee customerGroupFee;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
  private Set<ParkingTime> parkingTimes;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
  private Set<Invoice> invoices;

}
