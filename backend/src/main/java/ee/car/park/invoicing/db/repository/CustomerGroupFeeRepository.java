package ee.car.park.invoicing.db.repository;

import ee.car.park.invoicing.db.domain.CustomerGroupFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerGroupFeeRepository extends JpaRepository<CustomerGroupFee, Long> {
}