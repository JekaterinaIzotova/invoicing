package ee.car.park.invoicing.web.rest;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.Invoice;
import ee.car.park.invoicing.db.domain.ParkingTime;
import ee.car.park.invoicing.db.service.CustomerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@Slf4j
@RequestMapping("/customer")
@AllArgsConstructor
public class CustomerController {

  private final CustomerService customerService;

  @GetMapping("/{id}")
  public ResponseEntity<Customer> getCustomerById(@PathVariable Long id) {
    log.info(String.format("Get customer by id: %s", id));
    return ResponseEntity.ok().body(customerService.getCustomer(id));
  }

  @GetMapping("/{id}/invoice")
  public ResponseEntity<Set<Invoice>> getAllRelatedInvoices(@PathVariable Long id) {
    log.info(String.format("Get all related invoices. Customer id: %s", id));
    return ResponseEntity.ok().body(this.customerService.getAllRelatedInvoices(id));
  }

  @GetMapping("/{id}/parkingTime")
  public ResponseEntity<Set<ParkingTime>> getAllRelatedParkingTimes(@PathVariable Long id) {
    log.info(String.format("Get all related parking times. Customer id: %s", id));
    return ResponseEntity.ok().body(this.customerService.getAllRelatedParkingTimes(id));
  }

}

