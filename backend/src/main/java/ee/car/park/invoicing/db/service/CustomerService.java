package ee.car.park.invoicing.db.service;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.Invoice;
import ee.car.park.invoicing.db.domain.ParkingTime;
import ee.car.park.invoicing.db.domain.User;
import ee.car.park.invoicing.db.repository.CustomerRepository;
import ee.car.park.invoicing.enumeration.ObjectType;
import ee.car.park.invoicing.exceptions.ObjectNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor
public class CustomerService {

  private final CustomerRepository customerRepository;
  private final InvoiceService invoiceService;
  private final ParkingTimeService parkingTimeService;

  public Customer getCustomer(Long id) {
    return this.getCustomerById(id);
  }

  private Customer getCustomerById(Long id) {
    Optional<Customer> customer = this.customerRepository.findById(id);
    if (customer.isEmpty()) {
      throw new ObjectNotFoundException(ObjectType.CUSTOMER);
    }
    return customer.get();
  }

  protected Set<Customer> findAllByUser(User user) {
    return this.customerRepository.findAllByUser(user);
  }

  public Set<Invoice> getAllRelatedInvoices(Long id) {
    return this.invoiceService.findAllByCustomer(this.getCustomerById(id));
  }

  public Set<ParkingTime> getAllRelatedParkingTimes(Long id) {
    return this.parkingTimeService.findAllByCustomer(this.getCustomerById(id));
  }

}
