package ee.car.park.invoicing.exceptions;

import ee.car.park.invoicing.enumeration.ErrorCode;
import ee.car.park.invoicing.enumeration.ObjectType;
import lombok.Getter;

@Getter
public class ObjectNotFoundException extends InvoicingRestException {

	public ObjectNotFoundException(ObjectType objectType) {
		super(ErrorCode.OBJECT_NOT_FOUND, objectType);
	}

}
