package ee.car.park.invoicing.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum ObjectType {

  PARKING_HOUSE("parkingHouse"),
  USER("user"),
  CUSTOMER("customer"),
  INVOICE("invoice");

  String objectType;
  private static final Map<String, ObjectType> lookup = new HashMap<>();

  static {
    for (ObjectType objectType : ObjectType.values()) {
      lookup.put(objectType.value(), objectType);
    }
  }

  ObjectType(String objectType) {
    this.objectType = objectType;
  }

  public String value() {
    return this.objectType;
  }

}
