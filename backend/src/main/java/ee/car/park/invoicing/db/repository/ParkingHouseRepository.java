package ee.car.park.invoicing.db.repository;

import ee.car.park.invoicing.db.domain.ParkingHouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingHouseRepository extends JpaRepository<ParkingHouse, Long> {
}