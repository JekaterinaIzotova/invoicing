package ee.car.park.invoicing.db.service;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.Invoice;
import ee.car.park.invoicing.db.repository.CustomerRepository;
import ee.car.park.invoicing.db.repository.InvoiceRepository;
import ee.car.park.invoicing.dto.InvoiceDto;
import ee.car.park.invoicing.enumeration.ObjectType;
import ee.car.park.invoicing.exceptions.ObjectNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor
public class InvoiceService {

  private final InvoiceRepository invoiceRepository;
  private final CustomerRepository customerRepository;

  private final static Integer PAYMENT_DEADLINE_DAYS = 5;

  public Invoice getInvoice(Long id) {
    return this.getInvoiceById(id);
  }

  private Invoice getInvoiceById(Long id) {
    Optional<Invoice> invoice = this.invoiceRepository.findById(id);
    if (invoice.isEmpty()) {
      throw new ObjectNotFoundException(ObjectType.INVOICE);
    }
    return invoice.get();
  }

  protected Set<Invoice> findAllByCustomer(Customer customer) {
    return this.invoiceRepository.findAllByCustomer(customer);
  }

  public Invoice addInvoice(InvoiceDto invoiceDto) {
    Invoice invoice = new Invoice();
    return setAndSaveFromDto(invoice, invoiceDto);
  }

  private Invoice setAndSaveFromDto(Invoice invoice, InvoiceDto invoiceDto) {
    Optional<Customer> customerOptional = this.customerRepository.findById(invoiceDto.getCustomer().getId());
    if (customerOptional.isEmpty()) {
      throw new ObjectNotFoundException(ObjectType.CUSTOMER);
    }
    invoice.setCustomer(customerOptional.get());
    invoice.setPeriodStart(invoiceDto.getPeriodStart());
    invoice.setPeriodEnd(invoiceDto.getPeriodEnd());
    invoice.setPaymentDeadline(LocalDate.now().plusDays(PAYMENT_DEADLINE_DAYS));
    invoice.setInvoiceDate(LocalDate.now());
    invoice.setNote(invoiceDto.getNote());

// TODO
//    Double total = this.countInvoiceTotal();
    Double total = 102.33;
    invoice.setTotal(total);

    return this.invoiceRepository.save(invoice);
  }

}
