package ee.car.park.invoicing.db.service;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.User;
import ee.car.park.invoicing.db.repository.UserRepository;
import ee.car.park.invoicing.enumeration.ObjectType;
import ee.car.park.invoicing.exceptions.ObjectNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor
public class UserService {

	private final UserRepository userRepository;
	private final CustomerService customerService;

  public User getUser(Long id) {
    return this.getUserById(id);
  }

  private User getUserById(Long id) {
    Optional<User> user = this.userRepository.findById(id);
    if (user.isEmpty()) {
      throw new ObjectNotFoundException(ObjectType.USER);
    }
    return user.get();
  }

  public Set<Customer> getAllRelatedCustomers(Long id) {
    return this.customerService.findAllByUser(this.getUserById(id));
  }

}
