package ee.car.park.invoicing.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Builder
public class CustomerGroupFeeDto extends BasicDto {

  private String customerType;
  private Double monthlyFee;
  private Double feeDay;
  private Double feeNight;
  private Double maximumInvoice;

}
