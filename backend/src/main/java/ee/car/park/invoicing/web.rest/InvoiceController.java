package ee.car.park.invoicing.web.rest;

import ee.car.park.invoicing.db.domain.Invoice;
import ee.car.park.invoicing.db.service.InvoiceService;
import ee.car.park.invoicing.dto.InvoiceDto;
import ee.car.park.invoicing.validation.ValidationGroups;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/invoice")
@AllArgsConstructor
public class InvoiceController {

  private final InvoiceService invoiceService;

  @GetMapping("/{id}")
  public ResponseEntity<Invoice> getInvoiceById(@PathVariable Long id) {
    log.info(String.format("Get invoice by id: %s", id));
    return ResponseEntity.ok().body(invoiceService.getInvoice(id));
  }

  @PostMapping
  public ResponseEntity<Invoice> addNewInvoice(@Validated(ValidationGroups.Insert.class) @RequestBody InvoiceDto invoiceDto) {
    log.info("Create new invoice");
    return ResponseEntity.ok().body(this.invoiceService.addInvoice(invoiceDto));
  }

}

