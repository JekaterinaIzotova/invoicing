package ee.car.park.invoicing.web.rest;

import ee.car.park.invoicing.db.domain.ParkingHouse;
import ee.car.park.invoicing.db.service.ParkingHouseService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/parkingHouse")
@AllArgsConstructor
public class ParkingHouseController {

  private final ParkingHouseService parkingHouseService;

  @GetMapping("/{id}")
  public ResponseEntity<ParkingHouse> getParkingHouseById(@PathVariable Long id) {
    log.info(String.format("Get parking house by id: %s", id));
    return ResponseEntity.ok().body(parkingHouseService.getParkingHouse(id));
  }

}
