package ee.car.park.invoicing.db.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;

@Entity
@Getter @Setter
@SequenceGenerator(name = "default", sequenceName = "customer_group_fee_id_seq", allocationSize = 1)
public class CustomerGroupFee extends BaseEntity {

  @Column(name = "customer_type")
  private String customerType;

  @Column(name = "monthly_fee")
  private Double monthlyFee;

  @Column(name = "fee_day")
  private Double feeDay;

  @Column(name = "fee_night")
  private Double feeNight;

  @Column(name = "maximum_invoice")
  private Double maximumInvoice;

}
