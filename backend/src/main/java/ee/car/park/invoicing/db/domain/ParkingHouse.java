package ee.car.park.invoicing.db.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
@SequenceGenerator(name = "default", sequenceName = "parking_house_id_seq", allocationSize = 1)
public class ParkingHouse extends BaseEntity {

  @Column(name = "address")
  private String address;

  @Column(name = "country")
  private String country;

  @Column(name = "email")
  private String email;

  @Column(name = "phone")
  private String phone;

  @Column(name = "contact_person_name")
  private String contactPersonName;

}
