package ee.car.park.invoicing.db.repository;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

  @Query("select c from Customer c where c.user = :user")
  Set<Customer> findAllByUser(@Param("user") User user);

}