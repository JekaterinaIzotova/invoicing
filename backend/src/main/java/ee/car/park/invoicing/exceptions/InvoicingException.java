package ee.car.park.invoicing.exceptions;

import ee.car.park.invoicing.enumeration.ObjectType;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@SuppressWarnings("ClassWithoutNoArgConstructor")
@Getter
public class InvoicingException extends InvoicingRestException {

	private final HttpStatus httpStatus;

	public InvoicingException(HttpStatus httpStatus, String errorCode, ObjectType objectType) {
		super(errorCode, objectType);
		this.httpStatus = httpStatus;
	}

	// Default BAD_REQUEST
	public InvoicingException(String errorCode, ObjectType objectType) {
		super(errorCode, objectType);
		this.httpStatus = HttpStatus.BAD_REQUEST;
	}

	// Default BAD_REQUEST
	public InvoicingException(String errorCode) {
		super(errorCode, null);
		this.httpStatus = HttpStatus.BAD_REQUEST;
	}

}