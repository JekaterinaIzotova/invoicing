package ee.car.park.invoicing.enumeration;

public interface ErrorCode {

  String REQUIRED = "ErrorCode.required";
  String OBJECT_NOT_FOUND = "ErrorCode.objectNotFound";

}
