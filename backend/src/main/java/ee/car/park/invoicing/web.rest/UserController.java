package ee.car.park.invoicing.web.rest;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.User;
import ee.car.park.invoicing.db.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@Slf4j
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

  private final UserService userService;

  @GetMapping("/{id}")
  public ResponseEntity<User> getUserById(@PathVariable Long id) {
    log.info(String.format("Get user by id: %s", id));
    return ResponseEntity.ok().body(userService.getUser(id));
  }

  @GetMapping("/{id}/customer")
  public ResponseEntity<Set<Customer>> getAllRelatedCustomers(@PathVariable Long id) {
    log.info(String.format("Get all related customers. User id: %s", id));
    return ResponseEntity.ok().body(this.userService.getAllRelatedCustomers(id));
  }

}
