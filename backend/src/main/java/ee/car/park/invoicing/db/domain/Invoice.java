package ee.car.park.invoicing.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter @Setter
@SequenceGenerator(name = "default", sequenceName = "invoice_id_seq", allocationSize = 1)
public class Invoice extends BaseEntity {

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  private Customer customer;

  @Column(name = "total")
  private Double total;

  @Column(name = "period_start")
  private LocalDate periodStart;

  @Column(name = "period_end")
  private LocalDate periodEnd;

  @Column(name = "payment_deadline")
  private LocalDate paymentDeadline;

  @Column(name = "invoice_date")
  private LocalDate invoiceDate;

  @Column(name = "note")
  private String note;

}
