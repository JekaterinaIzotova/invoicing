package ee.car.park.invoicing.db.service;

import ee.car.park.invoicing.db.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class CustomerGroupFeeService {

	private final UserRepository userRepository;

}
