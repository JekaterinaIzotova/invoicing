package ee.car.park.invoicing.db.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter @Setter
@SequenceGenerator(name = "default", sequenceName = "user_id_seq", allocationSize = 1)
public class User extends BaseEntity {

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY)
  private ParkingHouse parkingHouse;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "email")
  private String email;

  @Column(name = "phone")
  private String phone;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
  private Set<Customer> customers;

  public String getFullName() {
    return String.format("%s %s", firstName, lastName);
  }

}
