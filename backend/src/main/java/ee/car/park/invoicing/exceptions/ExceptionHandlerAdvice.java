package ee.car.park.invoicing.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlerAdvice {

  @ExceptionHandler(InvoicingException.class)
  public List<Map<String, String>> handleInvoicingException(InvoicingException ex, HttpServletResponse response) {
    response.setStatus(ex.getHttpStatus().value());
    return getAndLogErrorResult(ex);
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(ObjectNotFoundException.class)
  public List<Map<String, String>> handleObjectNotFoundException(ObjectNotFoundException ex) {
    return getAndLogErrorResult(ex);
  }

  private List<Map<String, String>> getAndLogErrorResult(InvoicingRestException ex) {
    List<Map<String, String>> result = new ArrayList<>();
    Map<String, String> errors = new HashMap<>();
    String message = "";

    String errorCode = ex.getErrorCode();
    errors.put("errorCode", errorCode);

    if (ex.getObjectType() != null ) {
      String objectType = ex.getObjectType().value();
      errors.put("objectType", objectType);
      message = objectType;
    }

    result.add(errors);
    message += " " + errorCode;

    log.warn(message, ex);
    return result;
  }

}
