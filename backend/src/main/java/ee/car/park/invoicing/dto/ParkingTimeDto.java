package ee.car.park.invoicing.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter
@Builder
public class ParkingTimeDto extends BasicDto {

  private CustomerDto customer;
  private LocalDateTime startDateTime;
  private LocalDateTime endDateTime;

}
