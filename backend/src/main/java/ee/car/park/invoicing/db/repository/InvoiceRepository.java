package ee.car.park.invoicing.db.repository;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

  @Query("select i from Invoice i where i.customer = :customer")
  Set<Invoice> findAllByCustomer(@Param("customer") Customer customer);

}