package ee.car.park.invoicing.db;

import ee.car.park.invoicing.db.domain.*;
import ee.car.park.invoicing.dto.*;

import java.util.HashSet;
import java.util.Set;

public class InvoicingUtil {

  public static ParkingHouseDto convertParkingHouseToDto(ParkingHouse parkingHouse) {
    if (parkingHouse == null) {
      return null;
    }
    ParkingHouseDto parkingHouseDto = ParkingHouseDto.builder()
            .address(parkingHouse.getAddress())
            .country(parkingHouse.getCountry())
            .email(parkingHouse.getEmail())
            .phone(parkingHouse.getPhone())
            .contactPersonName(parkingHouse.getContactPersonName())
            .build();
    parkingHouseDto.setId(parkingHouse.getId());
    return parkingHouseDto;
  }

  public static UserDto convertUserToDto(User user) {
    if (user == null) {
      return null;
    }
    ParkingHouseDto parkingHouseDto = convertParkingHouseToDto(user.getParkingHouse());
    Set<CustomerDto> customerDtos = convertCustomerSetToDto(user.getCustomers());
    UserDto userDto = UserDto.builder()
            .parkingHouse(parkingHouseDto)
            .firstName(user.getFirstName())
            .lastName(user.getLastName())
            .email(user.getEmail())
            .phone(user.getPhone())
            .customers(customerDtos)
            .build();
    userDto.setId(user.getId());
    return userDto;
  }

  public static CustomerDto convertCustomerToDto(Customer customer) {
    if (customer == null) {
      return null;
    }
    UserDto userDto = convertUserToDto(customer.getUser());
    CustomerGroupFeeDto customerGroupFeeDto = convertCustomerGroupFeeToDto(customer.getCustomerGroupFee());
    Set<ParkingTimeDto> parkingTimeDtos = convertParkingTimeSetToDto(customer.getParkingTimes());
    Set<InvoiceDto> invoiceDtos = convertInvoiceSetToDto(customer.getInvoices());
    CustomerDto customerDto = CustomerDto.builder()
            .user(userDto)
            .carNumber(customer.getCarNumber())
            .customerGroupFee(customerGroupFeeDto)
            .parkingTimes(parkingTimeDtos)
            .invoices(invoiceDtos)
            .build();
    userDto.setId(customer.getId());
    return customerDto;
  }

  public static Set<CustomerDto> convertCustomerSetToDto(Set<Customer> customers) {
    if (customers == null) {
      return null;
    }
    Set<CustomerDto> customerDtos = new HashSet<>();
    for (Customer customer : customers) {
      CustomerDto customerDto = convertCustomerToDto(customer);
      customerDtos.add(customerDto);
    }
    return customerDtos;
  }

  public static CustomerGroupFeeDto convertCustomerGroupFeeToDto(CustomerGroupFee customerGroupFee) {
    if (customerGroupFee == null) {
      return null;
    }
    CustomerGroupFeeDto customerGroupFeeDto = CustomerGroupFeeDto.builder()
            .customerType(customerGroupFee.getCustomerType())
            .monthlyFee(customerGroupFee.getMonthlyFee())
            .feeDay(customerGroupFee.getFeeDay())
            .feeNight(customerGroupFee.getFeeNight())
            .maximumInvoice(customerGroupFee.getMaximumInvoice())
            .build();
    customerGroupFeeDto.setId(customerGroupFee.getId());
    return customerGroupFeeDto;
  }

  public static ParkingTimeDto convertParkingTimeToDto(ParkingTime parkingTime) {
    if (parkingTime == null) {
      return null;
    }
    CustomerDto customerDto = convertCustomerToDto(parkingTime.getCustomer());
    ParkingTimeDto parkingTimeDto = ParkingTimeDto.builder()
            .customer(customerDto)
            .startDateTime(parkingTime.getStartDateTime())
            .endDateTime(parkingTime.getEndDateTime())
            .build();
    parkingTimeDto.setId(parkingTime.getId());
    return parkingTimeDto;
  }

  public static Set<ParkingTimeDto> convertParkingTimeSetToDto(Set<ParkingTime> parkingTimes) {
    if (parkingTimes == null) {
      return null;
    }
    Set<ParkingTimeDto> parkingTimeDtos = new HashSet<>();
    for (ParkingTime parkingTime : parkingTimes) {
      ParkingTimeDto parkingTimeDto = convertParkingTimeToDto(parkingTime);
      parkingTimeDtos.add(parkingTimeDto);
    }
    return parkingTimeDtos;
  }

  public static InvoiceDto convertInvoiceToDto(Invoice invoice) {
    if (invoice == null) {
      return null;
    }
    CustomerDto customerDto = convertCustomerToDto(invoice.getCustomer());
    InvoiceDto invoiceDto = InvoiceDto.builder()
            .customer(customerDto)
            .total(invoice.getTotal())
            .periodStart(invoice.getPeriodStart())
            .periodEnd(invoice.getPeriodEnd())
            .paymentDeadline(invoice.getPaymentDeadline())
            .invoiceDate(invoice.getInvoiceDate())
            .note(invoice.getNote())
            .build();
    invoiceDto.setId(invoice.getId());
    return invoiceDto;
  }

  public static Set<InvoiceDto> convertInvoiceSetToDto(Set<Invoice> invoices) {
    if (invoices == null) {
      return null;
    }
    Set<InvoiceDto> invoiceDtos = new HashSet<>();
    for (Invoice invoice : invoices) {
      InvoiceDto invoiceDto = convertInvoiceToDto(invoice);
      invoiceDtos.add(invoiceDto);
    }
    return invoiceDtos;
  }

}
