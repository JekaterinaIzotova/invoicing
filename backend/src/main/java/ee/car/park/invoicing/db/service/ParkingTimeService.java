package ee.car.park.invoicing.db.service;

import ee.car.park.invoicing.db.domain.Customer;
import ee.car.park.invoicing.db.domain.ParkingTime;
import ee.car.park.invoicing.db.repository.ParkingTimeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Slf4j
@AllArgsConstructor
public class ParkingTimeService {

	private final ParkingTimeRepository parkingTimeRepository;

  protected Set<ParkingTime> findAllByCustomer(Customer customer) {
    return this.parkingTimeRepository.findAllByCustomer(customer);
  }

}
