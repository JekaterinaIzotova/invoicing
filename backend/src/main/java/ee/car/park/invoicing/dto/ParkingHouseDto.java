package ee.car.park.invoicing.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Builder
public class ParkingHouseDto extends BasicDto {

  private String address;
  private String country;
  private String email;
  private String phone;
  private String contactPersonName;

}
