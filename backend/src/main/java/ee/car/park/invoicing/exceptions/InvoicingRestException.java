package ee.car.park.invoicing.exceptions;

import ee.car.park.invoicing.enumeration.ObjectType;
import lombok.Getter;

@Getter
public class InvoicingRestException extends RuntimeException {

	private final String errorCode;
	private final ObjectType objectType;

	public InvoicingRestException(String errorCode, ObjectType objectType) {
		super(errorCode);
		this.errorCode = errorCode;
		this.objectType = objectType;
	}

}
