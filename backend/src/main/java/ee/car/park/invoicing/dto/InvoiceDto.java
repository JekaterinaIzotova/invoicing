package ee.car.park.invoicing.dto;

import ee.car.park.invoicing.enumeration.ErrorCode;
import ee.car.park.invoicing.validation.ValidationGroups;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter @Setter
@Builder
public class InvoiceDto extends BasicDto {

  @NotNull(
          groups = { ValidationGroups.Insert.class },
          message = ErrorCode.REQUIRED
  )
  private CustomerDto customer;

  private Double total;

  @NotNull(
          groups = { ValidationGroups.Insert.class },
          message = ErrorCode.REQUIRED
  )
  private LocalDate periodStart;

  @NotNull(
          groups = { ValidationGroups.Insert.class },
          message = ErrorCode.REQUIRED
  )
  private LocalDate periodEnd;

  private LocalDate paymentDeadline;

  private LocalDate invoiceDate;

  private String note;

}
