package ee.car.park.invoicing.validation;

import javax.validation.groups.Default;

public class ValidationGroups {

  private ValidationGroups() {
  }

  public interface Insert extends Default {}
  public interface Update extends Default {}

}
