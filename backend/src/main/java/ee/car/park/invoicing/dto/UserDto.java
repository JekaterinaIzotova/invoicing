package ee.car.park.invoicing.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter @Setter
@Builder
public class UserDto extends BasicDto {

  private ParkingHouseDto parkingHouse;
  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private Set<CustomerDto> customers;

}
