package ee.car.park.invoicing.db.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@MappedSuperclass
@Getter @Setter
public class BaseEntity {

  @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default")
  private Long id;

}
