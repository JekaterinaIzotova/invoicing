package ee.car.park.invoicing.db.service;

import ee.car.park.invoicing.db.domain.ParkingHouse;
import ee.car.park.invoicing.db.repository.ParkingHouseRepository;
import ee.car.park.invoicing.enumeration.ObjectType;
import ee.car.park.invoicing.exceptions.ObjectNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class ParkingHouseService {

	private final ParkingHouseRepository parkingHouseRepository;

  public ParkingHouse getParkingHouse(Long id) {
    return this.getParkingHouseById(id);
  }

  private ParkingHouse getParkingHouseById(Long id) {
    Optional<ParkingHouse> parkingHouse = this.parkingHouseRepository.findById(id);
    if (parkingHouse.isEmpty()) {
      throw new ObjectNotFoundException(ObjectType.PARKING_HOUSE);
    }
    return parkingHouse.get();
  }

}
