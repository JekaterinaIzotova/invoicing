package ee.car.park.invoicing.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter @Setter
@Builder
public class CustomerDto extends BasicDto {

  private UserDto user;
  private String carNumber;
  private CustomerGroupFeeDto customerGroupFee;
  private Set<ParkingTimeDto> parkingTimes;
  private Set<InvoiceDto> invoices;

}
