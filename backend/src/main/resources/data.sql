DROP TABLE IF EXISTS parking_house;
CREATE TABLE parking_house (
  id BIGINT NOT NULL,
  address VARCHAR(250),
  country VARCHAR(100),
  email VARCHAR(100),
  phone VARCHAR(100),
  contact_person_name VARCHAR(250),
  CONSTRAINT parking_house_pkey PRIMARY KEY (id));

DROP TABLE IF EXISTS user;
CREATE TABLE user (
  id BIGINT NOT NULL,
  parking_house_id BIGINT,
  first_name VARCHAR(100),
  last_name VARCHAR(100),
  email VARCHAR(100),
  phone VARCHAR(100),
  CONSTRAINT user_pkey PRIMARY KEY (id));

ALTER TABLE user 
ADD CONSTRAINT user_parking_house_fkey 
FOREIGN KEY (parking_house_id) 
REFERENCES parking_house (id) 
ON UPDATE NO ACTION ON DELETE NO ACTION;

DROP TABLE IF EXISTS customer_group_fee;
CREATE TABLE customer_group_fee (
  id BIGINT NOT NULL,
  customer_type VARCHAR(100),
  monthly_fee NUMERIC(10, 2),
  fee_day NUMERIC(10, 2),
  fee_night NUMERIC(10, 2),
  maximum_invoice NUMERIC(10, 2),
  CONSTRAINT customer_group_fee_pkey PRIMARY KEY (id));

DROP TABLE IF EXISTS customer;
CREATE TABLE customer (
  id BIGINT NOT NULL,
  user_id BIGINT,
  customer_group_fee_id BIGINT,
  car_number VARCHAR(100),
  CONSTRAINT customer_pkey PRIMARY KEY (id));

ALTER TABLE customer 
ADD CONSTRAINT customer_user_fkey 
FOREIGN KEY (user_id) 
REFERENCES user (id) 
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE customer 
ADD CONSTRAINT customer_customer_group_fee_fkey 
FOREIGN KEY (customer_group_fee_id) 
REFERENCES customer_group_fee (id) 
ON UPDATE NO ACTION ON DELETE NO ACTION;

DROP TABLE IF EXISTS parking_time;
CREATE TABLE parking_time (
  id BIGINT NOT NULL,
  customer_id BIGINT,
  start_date_time TIMESTAMP WITH TIME ZONE,
  end_date_time TIMESTAMP WITH TIME ZONE,
  CONSTRAINT parking_time_pkey PRIMARY KEY (id));

ALTER TABLE parking_time 
ADD CONSTRAINT parking_time_customer_fkey 
FOREIGN KEY (customer_id) 
REFERENCES customer (id) 
ON UPDATE NO ACTION ON DELETE NO ACTION;

DROP TABLE IF EXISTS invoice;
CREATE TABLE invoice (
  id BIGINT NOT NULL,
  customer_id BIGINT,
  total NUMERIC (8, 2),
  period_start DATE,
  period_end DATE,
  payment_deadline DATE,
  invoice_date DATE,
  note TEXT,
  CONSTRAINT invoice_pkey PRIMARY KEY (id));

ALTER TABLE invoice 
ADD CONSTRAINT invoice_customer_fkey 
FOREIGN KEY (customer_id) 
REFERENCES customer (id) 
ON UPDATE NO ACTION ON DELETE NO ACTION;


CREATE SEQUENCE IF NOT EXISTS parking_house_id_seq START WITH 1000 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS user_id_seq START WITH 1000 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS customer_group_fee_id_seq START WITH 1000 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS customer_id_seq START WITH 1000 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS parking_time_id_seq START WITH 1000 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;
CREATE SEQUENCE IF NOT EXISTS invoice_id_seq START WITH 1000 INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

INSERT INTO parking_house (id, address, country, email, phone, contact_person_name)
VALUES (nextval('parking_house_id_seq'), 'Building Street 17', 'Lithuania', 'park@mail.me', '(890)5456 2147', 'John Smith');

INSERT INTO user (id, parking_house_id, first_name, last_name, email, phone)
VALUES (nextval('user_id_seq'), 1000, 'Madis', 'Hunt', 'madis@mail.com', '(371)5455 9547');

INSERT INTO customer_group_fee (id, customer_type, monthly_fee, fee_day, fee_night, maximum_invoice)
VALUES 
(nextval('customer_group_fee_id_seq'), 'REGULAR', 0, 1.5, 1, null),
(nextval('customer_group_fee_id_seq'), 'PREMIUM', 20, 1, 0.75, 300);

INSERT INTO customer (id, user_id, car_number, customer_group_fee_id)
VALUES 
(nextval('customer_id_seq'), 1000, '654KJN', 1000),
(nextval('customer_id_seq'), 1000, '45AJJ', 1001),
(nextval('customer_id_seq'), 1000, '555JUL', 1000);

INSERT INTO parking_time (id, customer_id, start_date_time, end_date_time)
VALUES 
(nextval('parking_time_id_seq'), 1000, '2021-12-13 07:15:31', '2021-12-13 09:26:00'),
(nextval('parking_time_id_seq'), 1000, '2021-12-15 08:40:30', '2021-12-15 17:06:00'),
(nextval('parking_time_id_seq'), 1000, '2021-12-19 13:14:00', '2021-12-19 15:01:00'),
(nextval('parking_time_id_seq'), 1001, '2021-01-13 01:15:31', '2021-01-13 02:11:00'),
(nextval('parking_time_id_seq'), 1001, '2021-02-15 12:40:30', '2021-02-15 15:39:30'),
(nextval('parking_time_id_seq'), 1001, '2021-02-19 17:35:00', '2021-02-19 21:51:00');
